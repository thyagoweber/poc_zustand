import { GroceryList } from "@/components/organisms";
import { Container } from "@/components/atoms/Container";

const Home = (): JSX.Element => {
  return (
    <Container data-testid="home-root" as="main">
      <GroceryList />
    </Container>
  );
};

export default Home;
export { Home };
