import { render, screen } from "@testing-library/react";
import Home from ".";

test("Renders Home", () => {
  const component = render(<Home />);
  const rootElement = screen.getByTestId("home-root");

  expect(component).toMatchSnapshot();
  expect(rootElement.children.length).toBeGreaterThan(0);
});
