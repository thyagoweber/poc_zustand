import { render, screen } from "@testing-library/react";
import App from ".";

test("Renders App", () => {
  const component = render(<App />);
  const mainElement = screen.getByTestId("app-root");

  expect(component).toMatchSnapshot();
  expect(mainElement).toBeInTheDocument();
});
