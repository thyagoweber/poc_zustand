import { Home } from "../pages/Home";
import { Container } from "../components/atoms/Container";

function App() {
  return (
    <Container data-testid="app-root" p={[16]}>
      <Home />
    </Container>
  );
}

export default App;
