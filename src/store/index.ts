import create from "zustand";

import { groceryStore } from "@/components/organisms/GroceryList/store";

const useGroceryListStore = create(groceryStore);

export { useGroceryListStore };
