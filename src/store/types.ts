import { Optional } from "utility-types";

export type Grocery = {
  id: string;
  name: string;
  amount: number;
};

export type GroceryProperties = Optional<
  Omit<Grocery, "id">,
  "name" | "amount"
>;

export type Store = {
  groceries: {
    [x: string]: Grocery;
  };
  lastAddedGrocery: string;
  selectedGroceries: string[];
  addGrocery: (name: string, amount?: number) => void;
  removeGroceries: (ids?: string[]) => void;
  updateGrocery: (id: string, props: GroceryProperties) => void;
  setSelectedGroceries: ({
    checked,
    id,
  }: {
    checked: boolean;
    id: string;
  }) => void;
  getTotalAmount: () => number;
  getGroceriesList: () => Grocery[];
  isEmpty(): boolean;
};
