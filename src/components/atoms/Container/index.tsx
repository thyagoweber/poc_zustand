import {
  space,
  layout,
  display,
  flexbox,
  textAlign,
  LayoutProps,
  SpaceProps,
  DisplayProps,
  FlexboxProps,
  TextAlignProps,
} from "styled-system";
import styled from "styled-components";

const Container = styled.div<
  LayoutProps & SpaceProps & DisplayProps & FlexboxProps & TextAlignProps
>`
  ${space};
  ${layout};
  ${display};
  ${flexbox};
  ${textAlign};
`;

export { Container };
export default Container;
