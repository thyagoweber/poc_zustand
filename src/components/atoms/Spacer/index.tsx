import { layout, LayoutProps } from "styled-system";
import styled from "styled-components";

const Spacer = styled.div<LayoutProps>`
  ${layout};
`;

export { Spacer };
export default Spacer;
