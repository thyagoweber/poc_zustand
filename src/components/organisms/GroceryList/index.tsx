import { Container, Spacer } from "@/components/atoms";

import Form from "./components/Form";
import Actions from "./components/Actions";
import List from "./components/List";

const GroceryList = (): JSX.Element => {
  return (
    <Container data-testid="grocery-list-container">
      <h1 className="title has-text-centered">Grocery List</h1>

      <Form />

      <Actions />

      <Spacer height={[8]} />

      <List />
    </Container>
  );
};

export default GroceryList;
export { GroceryList };
