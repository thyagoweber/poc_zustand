import { combine } from "zustand/middleware";
import { createId } from "@/services/utils";

import { GroceryListStoreProperties, GroceryListStoreMethods } from "../types";

const groceryStore = combine<
  GroceryListStoreProperties,
  GroceryListStoreMethods
>(
  {
    groceries: {},
    selectedGroceries: [],
    lastAddedGrocery: "",
  },

  (set, get) => ({
    addGrocery(name, amount = 1) {
      const id = createId();

      set((state) => ({
        groceries: {
          ...state.groceries,
          ...{ [id]: { id, name, amount } },
        },
      }));
    },

    removeGroceries: (ids) =>
      set((state) => {
        if (!ids) {
          state.groceries = {};
        } else {
          ids.forEach((groceryId) => {
            delete state.groceries[groceryId];
          });
        }

        return {
          selectedGroceries: [],
          groceries: state.groceries,
        };
      }),

    updateGrocery: (id, props) =>
      set((state) => {
        state.groceries[id] = {
          ...state.groceries[id],
          ...props,
        };
        return { groceries: state.groceries };
      }),

    getTotalAmount: () =>
      Object.values(get().groceries).reduce(
        (acc, curr) => acc + curr.amount,
        0
      ),

    getGroceriesList: () => Object.values(get().groceries),

    setSelectedGroceries({ checked, id }) {
      const checkedItemsOnly = (id: string) => (value: string) => value !== id;

      set((state) => ({
        selectedGroceries: checked
          ? [...state.selectedGroceries, id]
          : state.selectedGroceries.filter(checkedItemsOnly(id)),
      }));
    },

    isEmpty: () => Object.keys(get().groceries).length === 0,
  })
);

export default groceryStore;
export { groceryStore };
