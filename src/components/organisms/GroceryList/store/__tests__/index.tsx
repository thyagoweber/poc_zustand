import {
  act,
  Renderer,
  renderHook,
  RenderHookResult,
} from "@testing-library/react-hooks";

import { zustandCreate, storeResetFns } from "@j/helpers";
import { groceryStore } from "..";
import { GroceryListStore } from "../../types";

import { getTotalGroceries, getId, getStore } from "./__test-utils__";

let store: RenderHookResult<
  unknown,
  GroceryListStore,
  Renderer<unknown>
> | null = null;

describe("Empty check", () => {
  beforeEach(() => {
    const useGroceryListStore = zustandCreate(groceryStore);
    store = renderHook(() => useGroceryListStore());
  });

  test("Should return true to a isEmpty() check", () => {
    let result;

    act(() => {
      result = store?.result.current.isEmpty();
    });

    expect(result).toBe(true);
  });

  afterEach(() => {
    act(() => storeResetFns.forEach((resetFn) => resetFn()));
    store = null;
  });
});

describe("Grocery item add", () => {
  let store: RenderHookResult<
    unknown,
    GroceryListStore,
    Renderer<unknown>
  > | null = null;

  beforeEach(() => {
    const useGroceryListStore = zustandCreate(groceryStore);
    store = renderHook(() => useGroceryListStore());
  });

  test("Should add a grocery item to the list", () => {
    act(() => {
      store?.result.current.addGrocery("Bananas", 3);
    });

    expect(getTotalGroceries(store?.result.current.groceries)).toBe(1);
  });

  afterEach(() => {
    act(() => storeResetFns.forEach((resetFn) => resetFn()));
    store = null;
  });
});

describe("Grocery item update", () => {
  let store: RenderHookResult<
    unknown,
    GroceryListStore,
    Renderer<unknown>
  > | null = null;

  beforeEach(() => {
    const useGroceryListStore = zustandCreate(groceryStore);
    store = renderHook(() => useGroceryListStore());
  });

  test("Should udpate a grocery item", () => {
    act(() => {
      getStore(store)?.addGrocery("Bananas", 3);
    });

    act(() => {
      getStore(store)?.updateGrocery(getId(store), { amount: 5 });
    });

    expect(getStore(store)?.groceries[getId(store)].amount).toBe(5);
  });

  afterEach(() => {
    act(() => storeResetFns.forEach((resetFn) => resetFn()));
    store = null;
  });
});

describe("Grocery items removal", () => {
  let store: RenderHookResult<
    unknown,
    GroceryListStore,
    Renderer<unknown>
  > | null;

  beforeEach(() => {
    const useGroceryListStore = zustandCreate(groceryStore);
    store = renderHook(() => useGroceryListStore());

    act(() => {
      getStore(store)?.addGrocery("Cidra", 10);
      getStore(store)?.addGrocery("Caninha 51", 5);
    });
  });

  test("Should remove one grocery item", () => {
    const theStore = getStore(store);

    act(() => {
      const groceries = Object.keys(theStore?.groceries || {});
      theStore?.removeGroceries([groceries[0]]);
    });

    expect(getTotalGroceries(theStore?.groceries)).toBe(1);
  });

  test("Should remove several grocery items", () => {
    const theStore = getStore(store);

    act(() => {
      const groceries = Object.keys(getStore(store)?.groceries || {});
      theStore?.removeGroceries([groceries[0], groceries[1]]);
    });

    expect(getTotalGroceries(theStore?.groceries)).toBe(0);
  });

  test("Should remove everything", async () => {
    const theStore = getStore(store);

    act(() => {
      theStore?.removeGroceries();
    });

    expect(theStore?.groceries).toBeDefined();
    expect(getTotalGroceries(theStore?.groceries)).toBe(0);
  });

  afterEach(() => {
    act(() => storeResetFns.forEach((resetFn) => resetFn()));
    store = null;
  });
});

describe("Grocery items selection", () => {
  let store: RenderHookResult<
    unknown,
    GroceryListStore,
    Renderer<unknown>
  > | null;

  beforeEach(() => {
    const useGroceryListStore = zustandCreate(groceryStore);
    store = renderHook(() => useGroceryListStore());

    act(() => {
      getStore(store)?.addGrocery("Cidra", 10);
      getStore(store)?.addGrocery("Caninha 51", 5);
    });
  });

  test("Should select a grocery", () => {
    act(() => {
      getStore(store)?.setSelectedGroceries({
        checked: true,
        id: getId(store),
      });
    });

    expect(getStore(store)?.selectedGroceries.length).toBe(1);
  });

  test("Should unselect a grocery", () => {
    act(() => {
      getStore(store)?.setSelectedGroceries({
        checked: false,
        id: getId(store),
      });
    });

    expect(getStore(store)?.selectedGroceries.length).toBe(0);
  });

  afterEach(() => {
    act(() => storeResetFns.forEach((resetFn) => resetFn()));
    store = null;
  });
});

describe("Total amount", () => {
  test("Should retrieve the total quantity of items added", () => {
    let store: RenderHookResult<
      unknown,
      GroceryListStore,
      Renderer<unknown>
    > | null = null;

    const useGroceryListStore = zustandCreate(groceryStore);
    store = renderHook(() => useGroceryListStore());

    act(() => {
      getStore(store)?.addGrocery("Bananas", 3);
      getStore(store)?.addGrocery("Maçãs", 3);
    });

    expect(getStore(store)?.getTotalAmount()).toBe(6);

    act(() => storeResetFns.forEach((resetFn) => resetFn()));
  });
});
