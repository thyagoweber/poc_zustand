import { Renderer, RenderHookResult } from "@testing-library/react-hooks";
import { Grocery, GroceryListStore } from "../../../types";

export const getStore = (
  store: RenderHookResult<unknown, GroceryListStore, Renderer<unknown>> | null
) => store?.result.current;

export const getTotalGroceries = (groceries?: { [x: string]: Grocery }) =>
  Object.keys(groceries || {}).length;

export const getId = (
  store: RenderHookResult<unknown, GroceryListStore, Renderer<unknown>> | null
) => Object.values(store?.result.current.groceries || {})[0].id;
