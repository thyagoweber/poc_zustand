import { render, screen } from "@testing-library/react";

import ListContainer from "..";

const methods = {
  isEmpty() {
    return methods.getGroceriesList().length === 0;
  },
  getGroceriesList: () => {
    return [
      { id: 1, name: "Carvão (saco)", amount: 1 },
      { id: 2, name: "Cerveja", amount: 100 },
      { id: 3, name: "Linguiça", amount: 100 },
    ];
  },
  updateGrocery: () => null,
  setSelectedGroceries: () => null,
  selectedGroceries: [],
  removeGroceries: () => null,
  getTotalAmount: () => 0,
};

describe("Grocery List Container", () => {
  beforeAll(() => {
    render(<ListContainer />);
  });

  test("Renders the grocery list container and its components", () => {
    const main = screen.getByTestId("grocery-list-container");
    const form = screen.getByTestId("grocery-add-form");
    const actions = screen.queryByTestId("grocery-list-actions");
    const heading = main.querySelector("h1");

    expect(main).toBeInTheDocument();
    expect(form).toBeInTheDocument();
    expect(actions).not.toBeInTheDocument();
    expect(heading).toBeInTheDocument();

    expect(main).toMatchSnapshot();
  });
});
