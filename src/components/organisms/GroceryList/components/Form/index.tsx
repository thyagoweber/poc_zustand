import shallow from "zustand/shallow";
import React from "react";

import { Container } from "@/components/atoms/Container";

import { useGroceryListStore } from "@/store";

const FormComponent = (): JSX.Element => {
  const store = useGroceryListStore(
    ({ addGrocery }) => ({ addGrocery }),
    shallow
  );

  const handleAddGrocery = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const form = event.currentTarget;
    const value = (form.elements[0] as HTMLInputElement).value;

    if (value) store.addGrocery(value);

    form.reset();
  };

  return (
    <Container mb={[32]} data-testid="grocery-add-form">
      <form onSubmit={handleAddGrocery}>
        <div className="field has-addons">
          <div className="control is-expanded">
            <input
              name="grocery"
              className="input is-large"
              type="search"
              placeholder="Add a grocery item"
            />
          </div>
          <div className="control">
            <button className="button is-large is-info">OK</button>
          </div>
        </div>
      </form>
    </Container>
  );
};

const Form = FormComponent;

export default Form;
export { Form };
