import { render, screen } from "@testing-library/react";
import Form from "..";

import { change, submit } from "./__test-utils__";

const mockAddGroceryFn = jest.fn();

jest.mock("@/store", () => {
  return {
    useGroceryListStore: () => {
      return {
        addGrocery: mockAddGroceryFn,
      };
    },
  };
});

describe("Add grocery form", () => {
  test("Renders Form", () => {
    const form = render(<Form />);
    const mainElement = screen.getByTestId("grocery-add-form");

    expect(form).toMatchSnapshot();
    expect(mainElement).toBeInTheDocument();
  });

  describe("Form submit", () => {
    let input: HTMLInputElement | null;
    let formElement: HTMLFormElement | null;
    let button: HTMLElement | null;

    beforeEach(() => {
      const form = render(<Form />);
      input = form.container.querySelector("input");
      formElement = form.container.querySelector("form");
      button = form.getByText("OK");
    });

    test("Should have a form, input and button elements", () => {
      expect(input).toBeDefined;
      expect(formElement).toBeDefined;
      expect(button).toBeDefined;
    });

    test("Should submit a grocery item", () => {
      let inputValue = "Bananas";

      if (!formElement || !input || !button) return;

      change(input, inputValue);

      const submitEvent = submit(formElement);

      expect(submitEvent.defaultPrevented).toBe(true);
      expect(mockAddGroceryFn).toBeCalled();
    });

    test("Should not submit with an invalid value", () => {
      let inputValue = "";

      if (!formElement || !input || !button) return;

      change(input, inputValue);

      submit(formElement);

      expect(mockAddGroceryFn).not.toBeCalled();
    });

    test("Should reset the form right after it's submitted", () => {
      if (!formElement || !input || !button) return;

      const onReset = jest.fn();

      formElement.reset = onReset;

      change(input, "");

      submit(formElement);

      expect(onReset).toBeCalled();
      expect(input.value).toBe("");
    });
  });
});
