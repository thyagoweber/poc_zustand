import { createEvent, fireEvent } from "@testing-library/react";

export const submit = (element: HTMLFormElement) => {
  const submitEvent = createEvent.submit(element);
  fireEvent(element, submitEvent);

  return submitEvent;
};

export const change = (input: HTMLInputElement, value: string) =>
  fireEvent.change(input, {
    target: { value },
  });
