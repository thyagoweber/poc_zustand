import {
  fireEvent,
  render,
  RenderResult,
  screen,
} from "@testing-library/react";
import * as hooks from "@/store";

import ActionsComponent from "..";

const methods = {
  removeGroceries: () => null,
  selectedGroceries: () => [],
  getTotalAmount: () => 0,
};

describe("Grocery List Actions", () => {
  describe("Removal", () => {
    const mockFn = jest.fn();
    let element: RenderResult;

    beforeEach(() => {
      jest.spyOn(hooks, "useGroceryListStore").mockImplementation(() => ({
        ...methods,
        selectedGroceries: ["1", "2", "3"],
        removeGroceries: mockFn,
        getTotalAmount: () => 5,
      }));

      element = render(<ActionsComponent />);
    });

    afterEach(() => {
      jest.resetAllMocks();
    });

    test("Should display 'Remove selected' button and remove selected items on having selected groceries", () => {
      const button = element.queryByTestId("grocery-remove-selected");

      expect(button).toBeInTheDocument();

      if (!button) return;

      fireEvent.click(button);

      expect(mockFn).toBeCalled();
    });

    test("Should display 'Remove all' button and remove all items on click", () => {
      const button = screen.queryByTestId("grocery-remove-all");

      expect(button).toBeInTheDocument();

      if (!button) return;

      fireEvent.click(button);

      expect(mockFn).toBeCalled();
    });
  });
});
