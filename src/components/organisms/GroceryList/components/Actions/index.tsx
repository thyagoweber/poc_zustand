import { Container, Spacer } from "@/components/atoms";
import React from "react";

import { useGroceryListStore } from "@/store";

import { Block } from "../Block";

const Actions = (): JSX.Element => {
  const store = useGroceryListStore(
    ({ removeGroceries, getTotalAmount, selectedGroceries }) => ({
      removeGroceries,
      getTotalAmount,
      selectedGroceries,
    })
  );

  const handleRemoveGroceries =
    (ids?: string[]) => (_event: React.SyntheticEvent) => {
      store.removeGroceries(ids);
    };

  const hasSelectedGroceries = React.useMemo(() => {
    return store.selectedGroceries.length > 0;
  }, [store.selectedGroceries.length]);

  const totalAmount = store.getTotalAmount();

  return (
    <Block data-testid="grocery-list-actions">
      <Block minHeight={[totalAmount !== 0 ? 40 : 0]}>
        <>
          {hasSelectedGroceries && (
            <button
              data-testid="grocery-remove-selected"
              className="button is-text"
              onClick={handleRemoveGroceries(store.selectedGroceries)}
            >
              Remove selected ({store.selectedGroceries.length})
            </button>
          )}

          <Spacer width={4} />

          {totalAmount > 1 && (
            <button
              data-testid="grocery-remove-all"
              className="button is-text"
              onClick={handleRemoveGroceries()}
            >
              Remove all
            </button>
          )}
        </>
      </Block>

      {totalAmount !== 0 && (
        <Container textAlign={["right"]} paddingX={[16]}>
          Total amount: {totalAmount}
        </Container>
      )}
    </Block>
  );
};

export default Actions;
export { Actions };
