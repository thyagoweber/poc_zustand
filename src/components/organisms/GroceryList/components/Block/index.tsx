import { Container } from "@/components/atoms/Container";

type BlockProps = {
  children: React.ReactNode;
  minHeight?: number[] | string[] | number | string;
};

const Block = ({ minHeight, children }: BlockProps): JSX.Element => {
  return (
    <Container
      display={["flex"]}
      alignItems={["center"]}
      justifyContent={["space-between"]}
      minHeight={minHeight}
    >
      {children}
    </Container>
  );
};

Block.defaultProps = {
  minHeight: 0,
};

export default Block;
export { Block };
