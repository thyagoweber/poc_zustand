import { Container, Spacer } from "@/components/atoms";
import { useGroceryListStore } from "@/store";

import { useDebouncedCallback } from "use-debounce";

import { Grocery, Amount } from "./styles";

import Block from "../Block";
import { GroceryListStore } from "../../types";

const selector = ({
  updateGrocery,
  setSelectedGroceries,
  removeGroceries,
  isEmpty,
  getGroceriesList,
}: GroceryListStore) => ({
  updateGrocery,
  setSelectedGroceries,
  removeGroceries,
  isEmpty,
  getGroceriesList,
});

const List = (): JSX.Element => {
  const {
    updateGrocery,
    setSelectedGroceries,
    removeGroceries,
    isEmpty,
    getGroceriesList,
  } = useGroceryListStore(selector);

  const handleGrocerySelection =
    (id: string) => (event: React.SyntheticEvent<HTMLInputElement>) => {
      setSelectedGroceries({ checked: event.currentTarget.checked, id });
    };

  const handleAmountChange = useDebouncedCallback(
    (
      amount: string,
      grocery: { id: string; name: string; amount: number }
    ): void => {
      updateGrocery(grocery.id, {
        amount: Number(amount),
      });
    },
    500
  );

  const handleRemoveGroceries =
    (ids?: string[]) => (_event: React.SyntheticEvent) => {
      removeGroceries(ids);
    };

  return (
    <Container data-testid="grocery-list">
      {isEmpty() && (
        <div data-testid="message" className="message is-info">
          <div className="message-body">Your grocery list is empty.</div>
        </div>
      )}

      <Container data-testid="groceries">
        {getGroceriesList().map((grocery) => (
          <Grocery data-testid={grocery.id} key={grocery.id}>
            <Container display={["flex"]}>
              <label className="checkbox">
                <input
                  type="checkbox"
                  onChange={handleGrocerySelection(grocery.id)}
                />
              </label>
              <Spacer width={[16]} />
              {grocery.name}
            </Container>

            <Block>
              <Amount
                max={100}
                min={1}
                pattern="\d*"
                defaultValue={grocery.amount}
                onChange={(event: React.SyntheticEvent<HTMLInputElement>) =>
                  handleAmountChange(event.currentTarget.value, grocery)
                }
              />
              <button
                onClick={handleRemoveGroceries([grocery.id])}
                className="delete"
              />
            </Block>
          </Grocery>
        ))}
      </Container>
    </Container>
  );
};
export default List;
