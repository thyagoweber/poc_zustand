import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import * as hooks from "@/store";

import List from "..";

const methods = {
  isEmpty() {
    return methods.getGroceriesList().length === 0;
  },
  getGroceriesList: () => {
    return [
      { id: 1, name: "Carvão (saco)", amount: 1 },
      { id: 2, name: "Cerveja", amount: 100 },
      { id: 3, name: "Linguiça", amount: 100 },
    ];
  },
  updateGrocery: () => null,
  setSelectedGroceries: () => null,
  removeGroceries: () => null,
};

describe("Grocery List", () => {
  test("Renders an empty grocery list", () => {
    jest.spyOn(hooks, "useGroceryListStore").mockImplementation(() => ({
      ...methods,
      getGroceriesList: () => [],
      isEmpty: () => true,
    }));

    const list = render(<List />);

    const mainElement = screen.getByTestId("grocery-list");
    const listContainer = list.getByTestId("groceries");
    const messageContainer = list.getByTestId("message");

    expect(mainElement).toBeInTheDocument();
    expect(listContainer.children.length).toBe(0);
    expect(messageContainer).toBeInTheDocument();
    expect(list).toMatchSnapshot();
  });

  test("Should select groceries", () => {
    const mockFn = jest.fn();

    jest.spyOn(hooks, "useGroceryListStore").mockImplementation(() => ({
      ...methods,
      setSelectedGroceries: mockFn,
    }));

    const list = render(<List />);

    list
      .getByTestId("groceries")
      .querySelectorAll("input")
      .forEach((item) => {
        fireEvent.click(item);
      });

    expect(mockFn).toBeCalledTimes(3);
  });

  test("Should update a grocery item", async () => {
    const mockFn = jest.fn();

    jest.spyOn(hooks, "useGroceryListStore").mockImplementation(() => ({
      ...methods,
      updateGrocery: mockFn,
    }));

    const list = render(<List />);

    const input = list.getByTestId("1").querySelectorAll("input")[1];

    expect(input).toBeDefined();

    if (!input) return;

    fireEvent.change(input, { target: { value: "5" } });

    await waitFor(() => expect(mockFn).toBeCalledWith(1, { amount: 5 }));
  });

  test("Should remove a grocery item", async () => {
    const mockFn = jest.fn();

    jest.spyOn(hooks, "useGroceryListStore").mockImplementation(() => ({
      ...methods,
      removeGroceries: mockFn,
    }));

    const list = render(<List />);

    const button = list.getByTestId("1").querySelector("button");
    expect(button).toBeDefined();

    if (!button) return;

    fireEvent.click(button);

    expect(mockFn).toBeCalled();
  });
});
