import styled from "styled-components";

export const Grocery = styled.div`
  background-color: white;
  border-radius: 4px;
  box-shadow: 0 4px 8px 4px rgb(10 10 10 / 10%), 0 0px 0 1px rgb(10 10 10 / 2%);
  color: #4a4a4a;
  max-width: 100%;
  position: relative;
  margin-bottom: 8px;
  padding: 8px 16px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const Amount = styled.input.attrs({
  type: "number",
  className: "input",
})`
  width: 72px;
  margin-right: 16px;
`;
