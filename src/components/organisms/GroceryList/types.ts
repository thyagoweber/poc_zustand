import { SetState, GetState } from "zustand";
import { Optional } from "utility-types";

export type Grocery = {
  id: string;
  name: string;
  amount: number;
};

export type GroceryProperties = Optional<
  Omit<Grocery, "id">,
  "name" | "amount"
>;

export type GroceryListStoreProperties = {
  groceries: {
    [x: string]: Grocery;
  };
  lastAddedGrocery: string;
  selectedGroceries: string[];
};

export type GroceryListStoreMethods = {
  addGrocery: (name: string, amount?: number) => void;
  removeGroceries: (ids?: string[]) => void;
  updateGrocery: (id: string, props: GroceryProperties) => void;
  setSelectedGroceries: ({
    checked,
    id,
  }: {
    checked: boolean;
    id: string;
  }) => void;
  getTotalAmount: () => number;
  getGroceriesList: () => Grocery[];
  isEmpty(): boolean;
};

export type GroceryListStore = GroceryListStoreProperties &
  GroceryListStoreMethods;
