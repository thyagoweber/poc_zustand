import actualCreate from "zustand";
import { StoreResetFunctions, ZustandCreateState } from "./types";

export const storeResetFns = new Set<StoreResetFunctions>();

export const zustandCreate = <T extends {}>(
  createState: ZustandCreateState<T>
) => {
  const store = actualCreate<T>(createState);
  const initialState = store.getState();
  storeResetFns.add(() => store.setState(initialState, true));
  return store;
};
