import { GetState, SetState, StoreApi, UseBoundStore } from "zustand";

declare type ZustandCreateState<T extends {}> = (
  set: SetState<T>,
  get: GetState<T>,
  api: any
) => T;

declare type StoreResetFunctions = () => void;

declare function zustandCreate<T extends {}>(
  params: ZustandCreateState<T>
): UseBoundStore<T, StoreApi<T>>;

declare global {
  namespace NodeJS {
    interface Global {
      [name: string]: any;
    }
  }
}
